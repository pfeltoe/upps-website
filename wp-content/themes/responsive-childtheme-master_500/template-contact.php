<?php

/*
Template Name: Template - Contact Page
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Site Front Page
 *
 * Note: You can overwrite front-page.php as well as any other Template in Child Theme.
 * Create the same file (name) include in /responsive-child-theme/ and you're all set to go!
 * @see            http://codex.wordpress.org/Child_Themes and
 *                 http://themeid.com/forum/topic/505/child-theme-example/
 *
 * @file           front-page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/front-page.php
 * @link           http://codex.wordpress.org/Template_Hierarchy
 * @since          available since Release 1.0
 */

/**
 * Globalize Theme Options
 */
$responsive_options = responsive_get_options();
/**
 * If front page is set to display the
 * blog posts index, include home.php;
 * otherwise, display static front page
 * content
 */
if ( 'posts' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
	get_template_part( 'home' );
} elseif ( 'page' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
	$template = get_post_meta( get_option( 'page_on_front' ), '_wp_page_template', true );
	$template = ( $template == 'default' ) ? 'index.php' : $template;
	locate_template( $template, true );
} else {

	get_header();

	//test for first install no database
	$db = get_option( 'responsive_theme_options' );
	//test if all options are empty so we can display default text if they are
	$empty = ( empty( $responsive_options['home_headline'] ) && empty( $responsive_options['home_subheadline'] ) && empty( $responsive_options['home_content_area'] ) ) ? false : true;
	?>
	

	<div class="featured">
		<h1>Contact Us</h1>
		<p>Thrive in a fast-paced mobile advertising environment. Uplette can help.</p>
	</div>
	<div class="wrapper_body">
		<div class="grid col-940">
		
		
			<div class="grid col-300">
				<h5>Contact</h5>
				<p></p>
				<p><a href="tel:1-877-629-5620">1.877.629.5620</a></p>
				<p><a href="mailto:info@uplette.com">info@uplette.com</a></p>
			</div>
			<div class="grid col-300">
				<h5>Toronto Office</h5>
				<p>639 Queen St W.</p>
				<p>Toronto, ON M5V 2B7</p>
			</div>
			<div class="grid col-300 fit">
				<h5>New York Office</h5>
				<p>72 Allen Street</p>
				<p>New York, NY 10002</p>
			</div>

			
		</div>
		<div class="contactform">
			<!-- <?php echo do_shortcode( '[contact-form-7 id="55" title="Contact Us"]' ); ?> -->
			<?php echo do_shortcode( '[contact-form-7 id="26" title="Contact Page"]' ); ?>
			
			
			
		</div>
	</div>
	

	<?php
	get_footer();
}
?>
