jQuery(document).ready(function($) {
	
	// Hiding Features Tabs 
	
	$('#tabs').tabs({
		hide: {
			effect: "fade",
			duration: 150
		},
		show: {
			effect: "fade",
			duration: 150,
		},
		activate: function(event, ui) {
			window.location.hash = ui.newPanel.attr('id');
			$('html, body').animate({
					scrollTop: $('#' + ui.newPanel.attr('id')).offset().top
			},500);
		}
	});
		
	// Add Tab name to URL
	
	var path = window.location.pathname;
	$('.navitem[name="' + path + '"]').addClass("active");
	
	$('a').click(function(){$('html,body').animate({
        scrollTop:$( $.attr(this,'href')).offset().top
        }, 500);
    return false;
});

});


