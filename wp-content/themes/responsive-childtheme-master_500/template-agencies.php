<?php

/*
Template Name: Template - Agencies
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Site Front Page
 *
 * Note: You can overwrite front-page.php as well as any other Template in Child Theme.
 * Create the same file (name) include in /responsive-child-theme/ and you're all set to go!
 * @see            http://codex.wordpress.org/Child_Themes and
 *                 http://themeid.com/forum/topic/505/child-theme-example/
 *
 * @file           front-page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/front-page.php
 * @link           http://codex.wordpress.org/Template_Hierarchy
 * @since          available since Release 1.0
 */

/**
 * Globalize Theme Options
 */
$responsive_options = responsive_get_options();
/**
 * If front page is set to display the
 * blog posts index, include home.php;
 * otherwise, display static front page
 * content
 */
if ( 'posts' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
	get_template_part( 'home' );
} elseif ( 'page' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
	$template = get_post_meta( get_option( 'page_on_front' ), '_wp_page_template', true );
	$template = ( $template == 'default' ) ? 'index.php' : $template;
	locate_template( $template, true );
} else {
	get_header();
	//test for first install no database
	$db = get_option( 'responsive_theme_options' );
	//test if all options are empty so we can display default text if they are
	$empty = ( empty( $responsive_options['home_headline'] ) && empty( $responsive_options['home_subheadline'] ) && empty( $responsive_options['home_content_area'] ) ) ? false : true;
	?>

<div class="wrapper_body wrapper_company">

 	<h1>Agencies</h1>
	<p>Improve ROI on your mobile ad spend with relevant dynamic, creative that converts!</p>
	<a href="/index.php/signup">TRY IT FOR FREE FOR 14 DAYS</a>
</div>

<div class="wrapper_body wrapper_white">
	<div class="benefit grid col-940">
		<div class="grid col-460">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/agencies/1.png">
		</div>
		<div class="grid col-460 fit">
			<h2>Improve engagement.</h2>
			<p>Relevance is key. Engage consumers with targeted content tailored through Uplette’s contextual engine.</p>
		</div>
	</div>
	<div class="benefit grid col-940">
		<div class="grid col-460">
			<h2>Build dynamic ads and landing pages in one location.</h2>
			<p>Stop creating hundreds of ads and and landing pages for each customer segment! Uplette’s editing interface provides a scalable solution to create relevant and dynamic ads and landing pages.  Deliver the right content to the right audience.</p> 
		</div>
		<div class="grid col-460 fit">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/agencies/2.png">
		</div>
	</div>
	<div class="benefit grid col-940">
		<div class="grid col-460">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/agencies/3.png">
		</div>
		<div class="grid col-460 fit">
			<h2>See who performed on what and why.</h2>
			<p>Our real-time reporting and analytics dashboard allows you to see how your campaign is performing in detail, understand consumer performance and behavior.</p>
		</div>
	</div>
	<div class="benefit grid col-940">
		<div class="grid col-700">
			<h2>Reporting.</h2>
			<p>Tired of excel spreadsheets and hundreds of emails back and forth? Us too! Send your clients beautiful reports on campaign progress, consumer behavior and ROI right from the dashboard.</p> 
		</div>
		<div class="grid col-220 fit">
			<img class="noline" src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/agencies-04.svg">
		</div>
	</div>
	<div class="benefit grid col-940">
		<div class="grid col-220">
			<img class="noline" src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/agencies-05.svg">
		</div>
		<div class="grid col-700 fit">
			<h2>Auto optimization.</h2>
			<p>Deliver content that converts. Uplette goes beyond traditional A/B testing, and interprets performance based on semantic groupings of content elements to provide you optimization flexibility while preserving the integrity of your brand message.</p>
		</div>
	</div>
</div>

<!--
<div class="wrapper_body">

	<h1>Case Study</h1>
	<p>Pepsi Taste Challenge</p>

</div>
-->

<?php
	get_footer();
}
?>
