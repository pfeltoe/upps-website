<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Blog Template
 *
Template Name: Blog Excerpt (summary)
 *
 * @file           blog-excerpt.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/blog-excerpt.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */

get_header();
?>

<div id="content-blog">

	<?php get_template_part( 'loop-header' ); ?>

	<?php
	global $wp_query, $paged;
	if( get_query_var( 'paged' ) ) {
		$paged = get_query_var( 'paged' );
	}
	elseif( get_query_var( 'page' ) ) {
		$paged = get_query_var( 'page' );
	}
	else {
		$paged = 1;
	}
	$blog_query = new WP_Query( array( 'post_type' => 'post', 'paged' => $paged ) );
	$temp_query = $wp_query;
	$wp_query = null;
	$wp_query = $blog_query;

	if( $blog_query->have_posts() ) :

		while( $blog_query->have_posts() ) : $blog_query->the_post();
			?>
			
			
			<?php $image_id = get_post_thumbnail_id(); ?>
			<?php $image_url = wp_get_attachment_image_src($image_id,'small');   ?>
			

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<span><?php the_date();?></span>
				<h1><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h1>
				<a href="<?php echo get_permalink(); ?>">
				<div class="blogfancy-background-image" style="background-image:url(<?php echo $image_url[0]; ?>);"></div></a>
				
					<div class="blogfancy-content">
						<div class="post-entry"><?php the_excerpt(); ?></div>
					</div>
				
			</div>
			
			
		<!-- Page Sorting -->
		<?php
		endwhile;

		if( $wp_query->max_num_pages > 1 ) :
			?>
			<div class="navigation">
				<div class="previous"><?php next_posts_link( __( '&#8249;', 'responsive' ), $wp_query->max_num_pages ); ?></div>
				<div class="next"><?php previous_posts_link( __( '&#8250;', 'responsive' ), $wp_query->max_num_pages ); ?></div>
			</div><!-- end of .navigation -->
		<?php
		endif;

	else :

		get_template_part( 'loop-no-posts' );

	endif;
	$wp_query = $temp_query;
	wp_reset_postdata();
	?>

</div><!-- end of #content-blog -->

<?php get_footer(); ?>
