<?php

/*
Template Name: Template - Use Cases
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Site Front Page
 *
 * Note: You can overwrite front-page.php as well as any other Template in Child Theme.
 * Create the same file (name) include in /responsive-child-theme/ and you're all set to go!
 * @see            http://codex.wordpress.org/Child_Themes and
 *                 http://themeid.com/forum/topic/505/child-theme-example/
 *
 * @file           front-page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/front-page.php
 * @link           http://codex.wordpress.org/Template_Hierarchy
 * @since          available since Release 1.0
 */

/**
 * Globalize Theme Options
 */
$responsive_options = responsive_get_options();
/**
 * If front page is set to display the
 * blog posts index, include home.php;
 * otherwise, display static front page
 * content
 */
if ( 'posts' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
	get_template_part( 'home' );
} elseif ( 'page' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
	$template = get_post_meta( get_option( 'page_on_front' ), '_wp_page_template', true );
	$template = ( $template == 'default' ) ? 'index.php' : $template;
	locate_template( $template, true );
} else {
	get_header();
	//test for first install no database
	$db = get_option( 'responsive_theme_options' );
	//test if all options are empty so we can display default text if they are
	$empty = ( empty( $responsive_options['home_headline'] ) && empty( $responsive_options['home_subheadline'] ) && empty( $responsive_options['home_content_area'] ) ) ? false : true;
	?>
	<div class="featured feat_usecases">
		<h1>Use Cases</h1>
		<p>A flexible solution for real-time bidding platforms, ad exchanges, agencies, brands, and more, Uplette integrates seamlessly into your existing workflow.</p>
	</div>
	<div id="tabs">
		<div class="wrapper_menu">
			<ul>
				<li><a href="#web-ads"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-usecases-webads.svg"><br>Web Ads</a></li>
				<li><a href="#mobile-ads"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-usecases-mobileads.svg"><br>Mobile Ads</a></li>
				<li><a href="#creative-agency"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-usecases-creativeagency.svg"><br>Creative Agencies</a></li>
				<li><a href="#rtb-adexchanges"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-usecases-rtbadex.svg"><br>RTBs and Ad Exchanges</a></li>
	    	</ul>
		</div>
		<div class="wrapper_body">
			<div id="web-ads" class="usecasesection">
				<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-usecases-webads-dark.svg">
				<h2>Web Ads</h2>
				<p class="cent">Uplette enables brands and advertisers to deliver dynamic and context-aware ads and landing pages to increase click-throughs, boost conversion rates, and enhance brand interactions.</p>
				<hr>
				<ul>
					<li>Deliver dynamic and context-aware display ads and landing pages based on consumer preferences and behaviours</li>
					<li>Control the user experience from initial ad click to post-ad conversion, and leverage consumer insights to automatically optimize content based on campaign performance</li>
					<li>Leverage Uplette's On-Premise solution to deploy Uplette-powered landing pages on your own website for full branding control</li>
				</ul>
			</div>
			<div id="mobile-ads" class="usecasesection">
				<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-usecases-mobileads-dark.svg">
				<h2>Mobile Ads</h2>
				<p class="cent">Uplette allows brands and advertisers to deliver smart post-ad content to improve click-through rates, conversion rates and targeting for the mobile advertising experience.</p>
				<hr>
				<ul>
					<li>Deliver dynamic and context-aware post-ad landing pages by leveraging insights on who your customers are and how they are converting</li>
					<li>Create scalable landing pages that use context-aware filters to deliver the right content to the right consumer through Uplette’s flexible editing interface</li>
					<li>Integrate Uplette into a real-time bidding platform or ad exchange to automatically optimize ad buying, ad delivery, and audience retargeting based on campaign performance</li>
				</ul>
			</div>
			<!--
<div id="publishers" class="usecasesection">
				<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-usecases-publishers-dark.svg">
				<h2>Publishers</h2>
				<p class="cent">Improve ROI for brands and advertisers with scalable and dynamic creative content that can be optimized based on campaign performance and objectives.</p>
				<hr>
				<ul>
					<li>Deliver dynamic and context-aware display ads and landing pages based on consumer preferences and behaviours</li>
					<li>Control the user experience from initial ad click to post-ad conversion, and leverage consumer insights to automatically optimize content based on campaign performance</li>
					<li>Leverage Uplette's on-premise solution to deploy Uplette-powered landing pages on your own website for full branding control</li>
				</ul>
			</div>
-->
			<div id="creative-agency" class="usecasesection">
				<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-usecases-creativeagency-dark.svg">
				<h2>Creative Agencies</h2>
				<p class="cent">Improve ROI for brands and advertisers with scalable and dynamic creative content that can be optimized based on campaign performance and objectives.</p>
				<hr>
				<ul>
					<li>Create scalable landing pages that use context-aware filters to dynamically deliver the right content to the right audience using Uplette’s flexible editing interface</li>
					<li>Automatically optimize content with real-time insights on who your audience is and how they are converting</li>
					<li>Access campaign reporting tools to validate ROI in real-time with Uplette’s unified user profiles, analytics, and feedback loop</li>
				</ul>
			</div>
			<div id="rtb-adexchanges" class="usecasesection">
				<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-usecases-rtbadex-dark.svg">
				<h2>RTBs and Ad Exchanges</h2>
				<p class="cent">Uplette integrates seamlessly into RTBs, DSPs, and ad exchanges to improve the mobile advertising experience from initial ad delivery to post-ad click.</p>
				<hr>
				<ul>
					<li>Track the experience post-ad click, and retarget consumers who are more likely to convert with content that is relevant to them</li>
					<li>Leverage consumer information and campaign performance insights to automatically optimize ad buying decisions, initial ad delivery, and audience retargeting with our real-time feedback loop</li>
					<li>Enhance your existing data by identifying each consumer, their preferences and behaviours in real-time with Uplette's unified user profiles and post-ad interaction insights</li>
				</ul>
			</div>
	    </div>
	</div>
	<?php
	get_footer();
}
?>
