<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Single Posts Template
 *
 *
 * @file           single.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/single.php
 * @link           http://codex.wordpress.org/Theme_Development#Single_Post_.28single.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>


<div id="content">


	<?php get_template_part( 'loop-header' ); ?>

	<?php if( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>
		
		<?php $image_id = get_post_thumbnail_id(); ?>
		<?php $image_url = wp_get_attachment_image_src($image_id,'small');   ?>
		
		
		
		<span><?php the_date();?></span>
			<h1><?php echo get_the_title(); ?></h1>
				<div class="blogfancy-background-image" style="background-image:url(<?php echo $image_url[0]; ?>);"></div>
				
					<div class="content"><?php the_content(); ?></div>
			
		<?php
		endwhile;

		get_template_part( 'loop-nav' );

	else :

		get_template_part( 'loop-no-posts' );

	endif;
	?>

</div><!-- end of #content -->

<div class="navigation">
				<div class="previous"><?php previous_post_link('%link', '&#8249;', TRUE); ?></div>
				<div class="next"><?php next_post_link('%link', '&#8250;', TRUE); ?></div>
		</div>

<?php get_footer(); ?>
