<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
exit;
}

/**
* Site Front Page
*
* Note: You can overwrite front-page.php as well as any other Template in Child Theme.
* Create the same file (name) include in /responsive-child-theme/ and you're all set to go!
* @see            http://codex.wordpress.org/Child_Themes and
*                 http://themeid.com/forum/topic/505/child-theme-example/
*
* @file           front-page.php
* @package        Responsive
* @author         Emil Uzelac
* @copyright      2003 - 2013 ThemeID
* @license        license.txt
* @version        Release: 1.0
* @filesource     wp-content/themes/responsive/front-page.php
* @link           http://codex.wordpress.org/Template_Hierarchy
* @since          available since Release 1.0
*/

/**
* Globalize Theme Options
*/
$responsive_options = responsive_get_options();
/**
* If front page is set to display the
* blog posts index, include home.php;
* otherwise, display static front page
* content
*/
if ( 'posts' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
get_template_part( 'home' );
} elseif ( 'page' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
$template = get_post_meta( get_option( 'page_on_front' ), '_wp_page_template', true );
$template = ( $template == 'default' ) ? 'index.php' : $template;
locate_template( $template, true );
} else {

get_header();

//test for first install no database
$db = get_option( 'responsive_theme_options' );
//test if all options are empty so we can display default text if they are
$empty = ( empty( $responsive_options['home_headline'] ) && empty( $responsive_options['home_subheadline'] ) && empty( $responsive_options['home_content_area'] ) ) ? false : true;
?>

	<div class="featured">
		<div class="grid col-940">
			<h1>Mobile advertising that learns</h1>
			<p>Design & create highly personalized, dynamic creative</p>
			<a class="free-trial" href="/index.php/signup">Try it for free</a>
			<div class="learn-more"><?php echo do_shortcode( '[video_lightbox_vimeo5 video_id="109591424" width="640" height="480" anchor="&nbsp;&nbsp;&nbsp;&nbsp; How it Works"]' ) ?></div>
		</div>
	</div>
	
	<div class="howitworks">
<!-- 		<h1>How Uplette Works</h1> -->
		<p class="larger">Create highly relevant, personalized advertising experiences for your audiences using Uplette’s self-service creative development and engagement platform. Our technology includes unique user identification, precision filtering and machine learning to determine what content gets delivered.</p>
	</div>
	
	<div class="agevsdsp">
		<div class="age">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/fronticon-age.svg">
			<h2>Brands & Agencies</h2>
			<p>Improve ROI on your mobile ad spend</p>
			<a href="/index.php/agencies">Learn More</a>
		</div>
		<div class="dsp">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/fronticon-dsp.svg">
			<h2>Ad-Exchanges & DSPs</h2>
			<p>Why Uplette should be your creative partner</p>
			<a href="/index.php/dsps">Learn More</a>
		</div>
	</div>
	
			

	
	<div class="benefits"><?php get_template_part( 'links', 'benefits' ); ?></div>

	<div class="clients">
		<div class="grid col-300">
			<?php get_template_part( 'links', 'clients' ); ?>
		</div>
		<div class="grid col-300">
			<?php get_template_part( 'links', 'partners' ); ?>
		</div>
		<div class="grid col-300 fit">
			<?php get_template_part( 'links', 'press' ); ?>
		</div>
	</div>
		
<?php get_footer();}?>
