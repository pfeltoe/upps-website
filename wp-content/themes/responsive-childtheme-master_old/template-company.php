<?php

/*
Template Name: Template - Company
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Site Front Page
 *
 * Note: You can overwrite front-page.php as well as any other Template in Child Theme.
 * Create the same file (name) include in /responsive-child-theme/ and you're all set to go!
 * @see            http://codex.wordpress.org/Child_Themes and
 *                 http://themeid.com/forum/topic/505/child-theme-example/
 *
 * @file           front-page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/front-page.php
 * @link           http://codex.wordpress.org/Template_Hierarchy
 * @since          available since Release 1.0
 */

/**
 * Globalize Theme Options
 */
$responsive_options = responsive_get_options();
/**
 * If front page is set to display the
 * blog posts index, include home.php;
 * otherwise, display static front page
 * content
 */
if ( 'posts' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
	get_template_part( 'home' );
} elseif ( 'page' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
	$template = get_post_meta( get_option( 'page_on_front' ), '_wp_page_template', true );
	$template = ( $template == 'default' ) ? 'index.php' : $template;
	locate_template( $template, true );
} else {
	get_header();
	//test for first install no database
	$db = get_option( 'responsive_theme_options' );
	//test if all options are empty so we can display default text if they are
	$empty = ( empty( $responsive_options['home_headline'] ) && empty( $responsive_options['home_subheadline'] ) && empty( $responsive_options['home_content_area'] ) ) ? false : true;
	?>

<div class="wrapper_body">

	<h1>Meet the team</h1>
	<p>The ladies and gentlemen that keep the ship running.</p>
	
</div>

<div class="wrapper_body wrapper_white">
	<ul class="team">
		<li>
			<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/headshots/em_amanda.jpg">
			<h1>Amanda Parker</h1>
			<h2>Co-Founder & CEO</h2>
			<ul class="social">
				<li class="twitter"><a href="https://twitter.com/uncannymandy" target="_blank">T</a></li>
				<li class="linkedin"><a href="http://ca.linkedin.com/in/parkeramanda" target="_blank">L</a></li>
				<li class="angellist"><a href="https://angel.co/amanda-parker" target="_blank">A</a></li>
			</ul>
		</li>
		<li>
			<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/headshots/em_alexey.jpg">
			<h1>Alexey Adamsky</h1>
			<h2>Co-Founder & CIO</h2>
			<ul class="social">
				<li class="twitter"><a href="https://twitter.com/alkasai" target="_blank">T</a></li>
				<li class="linkedin"><a href="http://ca.linkedin.com/in/alkasai" target="_blank">L</a></li>
				<li class="angellist"><a href="https://angel.co/alkasai" target="_blank">A</a></li>
			</ul>
		</li>
		<li>
			<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/headshots/em_kowsheek.png">
			<h1>Kowsheek Mahmood</h1>
			<h2>CTO</h2>
			<ul class="social">
				<li class="twitter"><a href="https://twitter.com/aredkid" target="_blank">T</a></li>
				<li class="linkedin"><a href="http://ca.linkedin.com/in/kowsheek" target="_blank">L</a></li>
				<li class="angellist"><a href="https://angel.co/aredkid" target="_blank">A</a></li>
			</ul>
		</li>
		<li>
			<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/headshots/em_paul.jpg">
			<h1>Paul Feltoe</h1>
			<h2>CDO</h2>
			<ul class="social">
				<li class="twitter"><a href="https://twitter.com/paulfeltoe" target="_blank">T</a></li>
				<li class="linkedin"><a href="http://ca.linkedin.com/in/paulfeltoe" target="_blank">L</a></li>
				<li class="angellist"><a href="https://angel.co/paulfeltoe" target="_blank">A</a></li>
			</ul>
		</li>
	</ul>
</div>
<?php
	get_footer();
}
?>
