

<div class="grid col-300">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/howitworks-01.svg">
	<h4>Build</h4>
	<p>Rapidly design & create highly personalized, dynamic creative. Drag your assets, set your filters and publish your campaign. No need to create multiple sets of creative!</p>
</div>

<div class="grid col-300">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/howitworks-02.svg">
	<h4>Optimize & Manage</h4>
	<p>Manage all of your campaign creative, landing pages, ads and assets through our interface and universal ad tag. Auto-optimize performance with sophisticated prioritization, reporting and audience targeting features.</p>
</div>

<div class="grid col-300 fit">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/howitworks-03.svg">
	<h4>Analyze & Report</h4>
	<p>Delve deep into who performed on what with greater insights than ever before. Powered by Uplette’s semantic tags, demographic filters, and social integration, the analytics engine is an invaluable tool for marketers and advertisers.</p>
</div>
   
 
