<?php

/*
Template Name: Template - Agencies
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Site Front Page
 *
 * Note: You can overwrite front-page.php as well as any other Template in Child Theme.
 * Create the same file (name) include in /responsive-child-theme/ and you're all set to go!
 * @see            http://codex.wordpress.org/Child_Themes and
 *                 http://themeid.com/forum/topic/505/child-theme-example/
 *
 * @file           front-page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/front-page.php
 * @link           http://codex.wordpress.org/Template_Hierarchy
 * @since          available since Release 1.0
 */

/**
 * Globalize Theme Options
 */
$responsive_options = responsive_get_options();
/**
 * If front page is set to display the
 * blog posts index, include home.php;
 * otherwise, display static front page
 * content
 */
if ( 'posts' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
	get_template_part( 'home' );
} elseif ( 'page' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
	$template = get_post_meta( get_option( 'page_on_front' ), '_wp_page_template', true );
	$template = ( $template == 'default' ) ? 'index.php' : $template;
	locate_template( $template, true );
} else {
	get_header();
	//test for first install no database
	$db = get_option( 'responsive_theme_options' );
	//test if all options are empty so we can display default text if they are
	$empty = ( empty( $responsive_options['home_headline'] ) && empty( $responsive_options['home_subheadline'] ) && empty( $responsive_options['home_content_area'] ) ) ? false : true;
	?>

<div class="wrapper_body wrapper_company">

	<h1>DSPs</h1>
	<p>Why Uplette should be your creative partner.</p>
	<a href="/index.php/signup">TRY IT FOR FREE FOR 14 DAYS</a>
</div>

<div class="wrapper_body wrapper_white">
	<div class="benefit grid col-940">
		<div class="grid col-220">
			<img class="noline" src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/dsps-01.svg">
		</div>
		<div class="grid col-700 fit">
			<h2>Universal ad tag & easy 3rd party integration.</h2>
			<p>Uplette's ad tag & API is compatible with any standard third-party platform or software solution, including real-time bidding platforms, programmatic buying platforms, data management platforms, ad exchanges, or CRM solutions to enable real-time campaign automation and optimization.</p>
		</div>
	</div>
	<div class="benefit grid col-940">
		<div class="grid col-700">
			<h2>Unified user profiles.</h2>
			<p>Uniquely identify each consumer. We identify the user, their interests, preferences and their behaviours to serve them more relevant content.</p> 
		</div>
		<div class="grid col-220 fit">
			<img class="noline" src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/dsps-02.svg">
		</div>
	</div>
	<div class="benefit grid col-940">
		<div class="grid col-220">
			<img class="noline" src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/dsps-03.svg">
		</div>
		<div class="grid col-700 fit">
			<h2>Feedback loop.</h2>
			<p>You’ve got awesome data and analytics, now what? Use the data collected in your campaigns for retargeting with your existing DSP & ad-exchange or use one of our awesome partners.</p>
		</div>
	</div>
</div>

<!--
<div class="wrapper_body">

	<h1>Case Study</h1>
	<p>Pepsi Taste Challenge</p>

</div>
-->

<?php
	get_footer();
}
?>
