<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Archive Template
 *
 *
 * @file           archive.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.1
 * @filesource     wp-content/themes/responsive/archive.php
 * @link           http://codex.wordpress.org/Theme_Development#Archive_.28archive.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>

<div id="content-archive" class="grid col-700">

	<h6 class="title-archive">Viewing Posts for <?php single_cat_title(); ?></h6>
	
	<?php if( have_posts() ) : ?>


		<?php while( have_posts() ) : the_post(); ?>

			<?php responsive_entry_before(); ?>
			
			
				
			<?php $image_id = get_post_thumbnail_id(); ?>
			<?php $image_url = wp_get_attachment_image_src($image_id,'small');   ?>
				

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="background-image:url(<?php echo $image_url[0]; ?>);">
				<h1><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h1>
				<span><?php the_date();?></span>
				<?php get_template_part( 'post-data' ); ?>
				<div class="post-entry"><?php the_excerpt(); ?></div>
			</div>
				
			<?php responsive_entry_after(); ?>

		<?php
		endwhile;

		get_template_part( 'loop-nav' );

	else :

		get_template_part( 'loop-no-posts' );

	endif;
	?>

</div><!-- end of #content-archive -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
