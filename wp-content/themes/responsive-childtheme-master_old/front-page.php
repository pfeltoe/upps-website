<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
exit;
}

/**
* Site Front Page
*
* Note: You can overwrite front-page.php as well as any other Template in Child Theme.
* Create the same file (name) include in /responsive-child-theme/ and you're all set to go!
* @see            http://codex.wordpress.org/Child_Themes and
*                 http://themeid.com/forum/topic/505/child-theme-example/
*
* @file           front-page.php
* @package        Responsive
* @author         Emil Uzelac
* @copyright      2003 - 2013 ThemeID
* @license        license.txt
* @version        Release: 1.0
* @filesource     wp-content/themes/responsive/front-page.php
* @link           http://codex.wordpress.org/Template_Hierarchy
* @since          available since Release 1.0
*/

/**
* Globalize Theme Options
*/
$responsive_options = responsive_get_options();
/**
* If front page is set to display the
* blog posts index, include home.php;
* otherwise, display static front page
* content
*/
if ( 'posts' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
get_template_part( 'home' );
} elseif ( 'page' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
$template = get_post_meta( get_option( 'page_on_front' ), '_wp_page_template', true );
$template = ( $template == 'default' ) ? 'index.php' : $template;
locate_template( $template, true );
} else {

get_header();

//test for first install no database
$db = get_option( 'responsive_theme_options' );
//test if all options are empty so we can display default text if they are
$empty = ( empty( $responsive_options['home_headline'] ) && empty( $responsive_options['home_subheadline'] ) && empty( $responsive_options['home_content_area'] ) ) ? false : true;
?>

	<div class="featured">
		<div class="grid col-460">
			<h1>Dynamic, highly personalized creative optimization for mobile content.</h1>
			<p>Maximize advertising response and engagement.</p>
			<a id="free-trial" href="/index.php/signup">TRY IT FOR FREE FOR 14 DAYS</a>
		</div>
		<div class="grid col-460 fit front-animation">
			<?php get_template_part( 'frontpage', 'animation' ); ?>
		</div>
	</div>
	<div class="agevsdsp">
		<a href="/index.php/agencies" class="age"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/fronticon-age.svg">For Brands & Agencies<span>READ MORE</span></a>
		<a href="/index.php/dsps" class="dsp"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/fronticon-dsp.svg">For DSPs<span>READ MORE</span></a>
	</div>
	<div class="howitworks">
		<h1>How it works</h1>
		<p class="larger">Create highly relevant, personalized advertising experiences for your audiences using Uplette’s self-service creative development and engagement platform. Our technology includes unique user identification, precision filtering and machine learning to determine what content gets delivered.</p>
				<div class="grid col-940">
	<img class="screenshots" src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits/screenshots.png">
</div>
		<?php get_template_part( 'links', 'howitworks' ); ?>
	</div>
	<div class="clients">
		<div class="grid col-300">
			<?php get_template_part( 'links', 'clients' ); ?>
		</div>
		<div class="grid col-300">
			<?php get_template_part( 'links', 'partners' ); ?>
		</div>
		<div class="grid col-300 fit">
			<?php get_template_part( 'links', 'press' ); ?>
		</div>
	</div>
		
<?php get_footer();}?>
