<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Footer Template
 *
 *
 * @file           footer.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.2
 * @filesource     wp-content/themes/responsive/footer.php
 * @link           http://codex.wordpress.org/Theme_Development#Footer_.28footer.php.29
 * @since          available since Release 1.0
 */

/*
 * Globalize Theme options
 */
global $responsive_options;
$responsive_options = responsive_get_options();
?>
<?php responsive_wrapper_bottom(); // after wrapper content hook ?>
</div><!-- end of #wrapper -->
<?php responsive_wrapper_end(); // after wrapper hook ?>
</div><!-- end of #container -->
<?php responsive_container_end(); // after container hook ?>

<div id="footer" class="clearfix">
	<?php responsive_footer_top(); ?>	
	
	
	
		
		<div class="footer-col grid col-220"> 
			<h1>UPLETTE</h1>
			
			<!-- Footer Menu -->
			
			<?php wp_nav_menu(
			array(
				'container'       => 'div',
				'container_class' => 'footer-nav',
				'fallback_cb'     => 'responsive_fallback_menu',
				'theme_location'  => 'foot-menu-upps'
			)
			);
		?>
		</div>
		<div class="footer-col grid col-220"> 
			<h1>FOLLOW US</h1>
			
			<!-- Footer Menu -->
			
			<?php wp_nav_menu(
			array(
				'container'       => 'div',
				'container_class' => 'footer-nav',
				'fallback_cb'     => 'responsive_fallback_menu',
				'theme_location'  => 'foot-menu-foll'
			)
			);
		?>
		</div>
		
		<div class="footer-col grid col-220"> 
			<h1>GET IN TOUCH</h1>
			
			<!-- Footer Menu -->
			
			<?php wp_nav_menu(
			array(
				'container'       => 'div',
				'container_class' => 'footer-nav',
				'fallback_cb'     => 'responsive_fallback_menu',
				'theme_location'  => 'foot-menu-comm'
			)
			);
		?>
		</div>
		<div class="footer-col grid col-220 fit"> 
			<h1>CONTACT</h1>
			
			<!-- Footer Menu -->
			
			<?php wp_nav_menu(
			array(
				'container'       => 'div',
				'container_class' => 'footer-nav',
				'fallback_cb'     => 'responsive_fallback_menu',
				'theme_location'  => 'foot-menu-cont'
			)
			);
		?>
		</div>
		
		
		
		
	<!-- end #footer-wrapper -->
	<?php responsive_footer_bottom(); ?>
</div><!-- end #footer -->
<?php responsive_footer_after(); ?>
<?php wp_footer(); ?>


<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scripts.js" type="text/javascript"></script>

<script type="text/javascript">
	var trackcmp_email = '';
	var trackcmp = document.createElement("script");
	trackcmp.async = true;
	trackcmp.type = 'text/javascript';
	trackcmp.src = '//trackcmp.net/visit?actid=251576199&e='+trackcmp_email +'&r='+encodeURIComponent(document.referrer)+'&u='+encodeURIComponent(window.location.href);
	var trackcmp_s = document.getElementsByTagName("script");
	if (trackcmp_s.length) {
		trackcmp_s[0].parentNode.appendChild(trackcmp);
	} else {
		var trackcmp_h = document.getElementsByTagName("head");
		trackcmp_h.length && trackcmp_h[0].appendChild(trackcmp);
	}
</script>

</body>
</html>