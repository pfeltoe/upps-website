<?php
function my_scripts_method() {
          wp_enqueue_script(
               'custom-script',
               get_stylesheet_directory_uri() . '/js/scripts.js',
               array('jquery')
          );
          wp_enqueue_script('jquery-ui-tabs');
          wp_enqueue_script('jquery-ui-accordion');
          wp_enqueue_script('jquery-effects-fade');
}
add_action('wp_enqueue_scripts', 'my_scripts_method');
?>
<?php
function register_my_menu() {
  	register_nav_menu('foot-menu-upps',__( 'Footer Menu - Uplette' ));
  	register_nav_menu('foot-menu-comm',__( 'Footer Menu - Community' ));
  	register_nav_menu('foot-menu-cont',__( 'Footer Menu - Contact' ));
  	register_nav_menu('foot-menu-foll',__( 'Footer Menu - Follow Us' ));
  	register_nav_menu('foot-menu-lega',__( 'Footer Menu - Legal' ));
}
	add_action( 'init', 'register_my_menu' );
?>
<?php
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
?>