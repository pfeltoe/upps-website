<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'uplette');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*T.3rK|.4O;{^>{A8+Ha?r8|Z,,|v}+rr%SL+@;/*-J+ &A&dwKz`gA+Rd:+j1@=');
define('SECURE_AUTH_KEY',  '|qe_5aZMIt7->c;bbp+b@SBY:=w)^ZsUBVPpL{e6`y-RH8gj.4fs!vr`#:C39P+^');
define('LOGGED_IN_KEY',    'LI<$Q/ -91&9%@h7}]vE^abF)pgtoJwoF9-o=yn]>F7MpzdY6M1S}Vb*yWK-WRC&');
define('NONCE_KEY',        'kVN46}svhJ71wo7sH+dbOh{uuDhs^.Fw03_+Wy|Qm!C?~o7@VCBEp= 2t}84y`~7');
define('AUTH_SALT',        '5qdE!y*#n:B5ALKf}m,6Rr8:]xA+WEbS*5f.[U`D_w@(w9N8Rv^Sa]f^p3aW5X~l');
define('SECURE_AUTH_SALT', '5`^~=0Ou|Rcqvi>Dy>Y-_2bJ>l1/ZfjH{4ctP+tekq-$L4Re0C]t8 d,+oK4VTv8');
define('LOGGED_IN_SALT',   'iA(V#Q?Fl/&MIxZB0kQ:Gm @=*$}YopN9b5HpXYsW7F|!E*kCpepItc1*%k|/vEY');
define('NONCE_SALT',       '|sdQ>&L:EZ),rPnJBXMdQL+]R6D]j=<t`LD10,x{;GDmq{6Hc}PC[Tl=p+#56Q< ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to Canadian English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * en_CA.mo to wp-content/languages and set WPLANG to 'en_CA' to enable Canadian
 * English language support.
 */
define('WPLANG', 'en_CA');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
